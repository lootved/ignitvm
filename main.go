package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/user"
	"strings"
)

var home string

var baseDir = "/srv"

func main() {
	c, err := Connect("10.19.10.220")
	if err != nil {
		log.Fatalln(err)
	}
	if err != nil {
		log.Fatalln(err)
	}

	r, _ := c.Run("echo $HOME")
	log.Println(r)
	//initUser()

	// installDebian()
}

func installDebian() {
	wdir := home + "/dev"

	if _, err := os.Stat("/usr/sbin/debootstrap"); errors.Is(err, os.ErrNotExist) {

		cmd("mkdir -p " + wdir)
		if err := os.Chdir(wdir); err != nil {
			panic(err)
		}

		archive := wdir + "/deb.tgz"
		url := "https://salsa.debian.org/installer-team/debootstrap/-/archive/master/debootstrap-master.tar.gz"

		cmd("wget -O " + archive + " " + url)
		cmd("tar -xf " + archive)

		if err := os.Chdir(wdir + "/debootstrap-master"); err != nil {
			panic(err)
		}
		scmd("make install")
	}
	bootstrapDir := baseDir + "/deb"
	ver := "bookworm"
	if _, err := os.Stat(bootstrapDir); errors.Is(err, os.ErrNotExist) {
		cmd("debootstrap --no-check-gpg --download-only --arch amd64 " + ver + " " + bootstrapDir)
	}
	scmd("debootstrap --no-check-gpg --arch amd64 " + ver + " " + bootstrapDir)
	usr, _ := user.Current()
	uid := usr.Uid
	gid := usr.Gid
	scmd("chown -R " + uid + ":" + gid + " " + bootstrapDir)
	cmd("cp /etc/resolv.conf " + bootstrapDir + "/etc")
}

func initUser() {
	scmd("mkdir -p /etc/skel/.ssh")
	scmd("cp " + home + "/.ssh/authorized_keys /etc/skel/.ssh")
	user := "a"
	scmd("userdel -rf " + user)
	scmd("useradd -G localadmin -m -d " + baseDir + "/" + user + " -s /bin/bash " + user)

	scmd("chmod 777 /srv")
}
func cmd(val string) bool {
	fmt.Println(val)
	args := strings.Split(val, " ")
	c := exec.Command(args[0], args[1:]...)
	c.Stdout = os.Stdout
	c.Stdin = os.Stdin
	c.Stderr = os.Stderr

	err := c.Run()

	if err != nil {
		fmt.Println(err)
		return false
	}
	return true

}

func scmd(val string) bool {
	return cmd("sudo " + val)
}
