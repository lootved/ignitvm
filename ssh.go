package main

import (
	"fmt"
	"github.com/melbahja/goph"
	"golang.org/x/crypto/ssh/terminal"
	"os"
	"os/user"
	"syscall"
)

const USER = "a"

type Client struct {
	c goph.Client
}

func (c *Client) Run(cmd string) (string, error) {
	val, err := c.c.Run(cmd)
	if err != nil {
		return "", err
	}
	return string(val), nil

}

func Connect(host string) (*Client, error) {
	user, err := user.Current()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	home := user.HomeDir

	key := home + "/.ssh/new/dev"

	err, cli := connectKey(USER, key, host)

	if err != nil {

		for i := 0; i < 3; i++ {
			err, cli = connectPwd(user.Username, host)
			if err == nil {
				break
			}
		}
	}

	return &Client{c: *cli}, err
}

func connectPwd(userName, host string) (error, *goph.Client) {
	fmt.Println("provide password for user:", userName)
	bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))
	pass := string(bytePassword)

	auth := goph.Password(pass)

	client, err := goph.NewUnknown(userName, host, auth)
	if err != nil {
		fmt.Println(err)
		return err, nil

	}
	return nil, client

}

func connectKey(userName, key, host string) (error, *goph.Client) {

	auth, err := goph.Key(key, "")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	client, err := goph.NewUnknown(userName, host, auth)
	if err != nil {
		return err, nil

	}
	return nil, client

}
